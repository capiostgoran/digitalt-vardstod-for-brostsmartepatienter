function openCity(evt, cityName) {
    centralBild.style.display = "none";
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

function openCityS(evt, tabName) {
    // Declare all variables
    var i, tabcontentS, tablinksS;

    // Get all elements with class="tabcontent" and hide them
    tabcontentS = document.getElementsByClassName("tabcontentS");
    for (i = 0; i < tabcontentS.length; i++) {
        tabcontentS[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinksS = document.getElementsByClassName("tablinksS");
    for (i = 0; i < tablinksS.length; i++) {
        tablinksS[i].className = tablinksS[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabName).style.display = "block";
    /*evt.currentTarget.className += " active";*/
}


function on() {
    document.getElementById("overlay").style.display = "block";
}

function off() {
    document.getElementById("overlay").style.display = "none";
}

$(function () {
    $('#unCheck').click(function () {
        $('.tabSymptoms').find('input:checkbox').prop('checked', false);
    });
});

$(function () {
    $('#heartTrop').click(function () {
        $('#hrtScore').click();
    });
});
$(function () {
    $('#heartTrop1').click(function () {
        $('#hrtScore').click();
    });
});
$(function () {
    $('#bedSympt').click(function () {
        $('#defaultOpen').click();
    });
});

$(function () {
    $('#utrCvd').click(function () {
        $('#angTp').click();
    });
});

$(function () {
    $('#inlBesl').click(function () {
        $('#inVal').click();
    });
});

$(function () {
    $('#diagnCad').click(function () {
        $('#angKlass').click();
    });
});
/*
$(function () {
    $('#kontInfo').click(function () {
        $('#surveyMonkey').click();
    });
});
*/
$(function () {
    $('#grScore').click(function () {
        $('#graScore').click();
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var lok = $('input[name="typLokch"]:checked').length;
        if (lok > 0) {
            $('.checkSamman [value="angLokTyp"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angLokTyp"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var alok = $('input[name="atypLokch"]:checked').length;
        if (alok > 0) {
            $('.checkSamman [value="angLok"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angLok"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var kar = $('input[name="typKarakt"]:checked').length;
        if (kar > 0) {
            $('.checkSamman [value="angKarTyp"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angKarTyp"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var akar = $('input[name="atypKarakt"]:checked').length;
        if (akar > 0) {
            $('.checkSamman [value="angKar"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angKar"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var exe = $('input[name="typExertch"]:checked').length;
        if (exe > 0) {
            $('.checkSamman [value="angFlpTyp"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angFlpTyp"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var aexe = $('input[name="atypExertch"]:checked').length;
        if (aexe > 0) {
            $('.checkSamman [value="angFlp"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angFlp"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var dur = $('input[name="typDurat"]:checked').length;
        if (dur > 0) {
            $('.checkSamman [value="angDuratTyp"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angDuratTyp"]').prop('checked', false);
        }
    });
});


$(function () {
    $('.tabSymptoms').click(function () {
        var adur = $('input[name="atypDurat"]:checked').length;
        if (adur > 0) {
            $('.checkSamman [value="angDurat"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angDurat"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var tvar = $('input[name="typVar"]:checked').length;
        if (tvar > 0) {
            $('.checkSamman [value="angVariantTyp"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angVariantTyp"]').prop('checked', false);
        }
    });
});

$(function () {
    $('.tabSymptoms').click(function () {
        var atvar = $('input[name="atypVar"]:checked').length;
        if (atvar > 0) {
            $('.checkSamman [value="angVariant"]').prop('checked', true);
        } else {
            $('.checkSamman [value="angVariant"]').prop('checked', false);
        }
    });
});

$(function () {
    $('#nextProv').click(function () {
        $('#troponin2').val($('#troponin :selected').val());
    });
});

function heartScore() {
    var a = document.getElementById("ageOld").value;
    var h = document.getElementById("misstanke").value;
    var r = $('input[name="risk"]:checked').length;
    var cvdR = $('input[name="cvd"]:checked').length;

    var rf = riskFactors();
    function riskFactors() {
        if (r > 2 || cvdR == 1) {
            rf = 2;
        } else if (r == 0) {
            rf = 0;
        } else {
            rf = 1;
        }
        return rf;
    }
    var e = document.getElementById("ekg").value;
    var t = document.getElementById("troponin").value;
    var tV = tropValue();
    function tropValue() {
        if (t == 3) {
            tV = 2;
        } else if (t == 2) {
            tV = 1;
        } else {
            tV = 0;
        }
        return tV;
    }

    var sum = Number(a) + Number(h) + rf + Number(e) + tV;

    var path = patPlace();
    function patPlace() {
        if (sum < 4) {
            path = "låg risk för allvarliga hjärtkärlhändelser.";
            document.getElementById('btnHscore').style.backgroundColor = "green";
            document.getElementById('score').style.borderColor = "green";
        } else if (sum > 6) {
            path = "hög risk för allvarliga hjärtkärlhändelser, indikation för inneliggande vård.";
            document.getElementById('btnHscore').style.backgroundColor = "red";
            document.getElementById('score').style.borderColor = "red";
        } else {
            path = "medel risk för allvarliga hjärtkärlhändelser, överväg inläggning.";
            document.getElementById('btnHscore').style.backgroundColor = "orange";
            document.getElementById('score').style.borderColor = "orange";
        }
        return path;
    }
    document.getElementById("score").innerHTML = "HEART score" + " " + sum + "," + " " + path;
}

function clearTrop(form) {
    form.ageOld.value = "1";
    form.misstanke.value = "0";
    form.ekg.value = "0";
    form.troponin.value = "1";
    document.getElementById("score").innerHTML = "";
    $('#heartCalc').find('input:checkbox').prop('checked', false);
    document.getElementById('btnHscore').style.backgroundColor = "lightslategrey";
    document.getElementById('score').style.borderColor = "";
}

function skrivSvar() {
    var resultat;
    var count = $('input[name="anginaTyp"]:checked').length;
    switch (count) {
        case 1:
            resultat = "Icke-anginösa symtom.";
            document.getElementById('btnAngina').style.backgroundColor = "green";
            break;
        case 2:
            resultat = "Misstänkt atypisk angina.";
            document.getElementById('btnAngina').style.backgroundColor = "orange";
            break;
        case 3:
            resultat = "Misstänkt typisk angina.";
            document.getElementById('btnAngina').style.backgroundColor = "red";
            break;
        case 0:
            resultat = "Komplettera med beskrivning av symtom.";
    }
    document.getElementById("svarAngina").innerHTML = resultat;
}

function clearAngina() {
    document.getElementById("age").value = "";
    document.getElementById('sex').value = "1";
    document.getElementById("svarAngina").innerHTML = "";
    document.getElementById('ptpAngina').innerHTML = "";
    document.getElementById('vidareUtr').innerHTML = "";
    document.getElementById('addInfo').innerHTML = "";
    document.getElementById('demoMinus').innerHTML = "";
    document.getElementById('demoPlus').innerHTML = "";
    $('#anginaTp').find('input:checkbox').prop('checked', false);
    document.getElementById('btnAngina').style.backgroundColor = "lightslategrey";
    document.getElementById('btnPtp').style.backgroundColor = "lightslategrey";
    odds = 0;
}

var odds = 0;
function cadScore() {
    var a = document.getElementById("age").value;
    var s = document.getElementById("sex").value;
    var ang = $('input[name="anginaTyp"]:checked').length;

    if (ang == 0 && a.length == 0) {
        document.getElementById("addInfo").innerHTML = "Komplettera med beskrivning av symtom och riskfaktorer.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    } if (a.length == 0) {
        document.getElementById("addInfo").innerHTML = "Komplettera med beskrivning av riskfaktorer.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    } if (ang == 0) {
        document.getElementById("addInfo").innerHTML = "Komplettera med beskrivning av symtom.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    }

    var angRes;
    switch (ang) {
        case 1:
            angRes = 0;
            break;
        case 2:
            angRes = 0.633;
            break;
        case 3:
            angRes = 1.998;
    }

    var typDiab;
    if (ang == 3 && d == 1) {
        typDiab = -0.402;
    } else {
        typDiab = 0;
    }

    var logit = 0.062 * a + 1.332 * s + angRes + 0.828 * d + typDiab + 0.338 * htn + 0.422 * hlpd + 0.461 * sm - 7.539;
    odds = Math.exp(logit);
    var prob = Math.round(100 * odds / (1 + odds));
    if (prob < 15) {
        document.getElementById('btnPtp').style.backgroundColor = "green";
    } else if (prob >= 15 && prob <= 65) {
        document.getElementById('btnPtp').style.backgroundColor = "#ffc61a";
    } else if (prob >= 65 && prob <= 85) {
        document.getElementById('btnPtp').style.backgroundColor = "orange";
    } else {
        document.getElementById('btnPtp').style.backgroundColor = "red";
    }
    document.getElementById("addInfo").innerHTML = "";
    document.getElementById('ptpAngina').innerHTML = "Sannolikhet för kranskärlssjukdom är " + prob + " %.";
    document.getElementById('vidareUtr').innerHTML = "";
    document.getElementById('demoMinus').innerHTML = "";
    document.getElementById('demoPlus').innerHTML = "";
    return odds;
}

function fortUtred() {
    var o = odds;
    var a = document.getElementById("age").value;
    var ang = $('input[name="anginaTyp"]:checked').length;
    if (ang == 0 && a.length == 0) {
        document.getElementById("addInfo").innerHTML = "Komplettera med beskrivning av symtom och riskfaktorer.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    } if (a.length == 0) {
        document.getElementById("addInfo").innerHTML = "Komplettera med beskrivning av riskfaktorer.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    } if (ang == 0) {
        document.getElementById("addInfo").innerHTML = "Komplettera med beskrivning av symtom.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    } if (o == 0) {
        document.getElementById("addInfo").innerHTML = "Beräkna 'Sannolikhet för kranskärlssjukdom'.";
        return;
    }

    var p = Math.round(100 * o / (1 + o));
    var test;
    if (p < 15) {
        test = "utredning av icke-kardiell genes till bröstsmärta.";
    } else if (p >= 15 && p <= 65) {
        test = "arbets-EKG eller DT kranskärl.";
    } else if (p >= 65 && p <= 85) {
        test = "bilddiagnostik, DT kranskärl eller kompletterande riskstratifiering med arbets-EKG.";
    } else {
        test = "coronarangiografi."
    }

    var postOddsplus = 1.53 * o;
    var postOddsminus = 0.68 * o;
/*    var postOddsplus = 3.8 * o;
    var postOddsminus = 0.6 * o;*/
    var probPlus = Math.round(100 * postOddsplus / (1 + postOddsplus));
    var probMinus = Math.round(100 * postOddsminus / (1 + postOddsminus));
    document.getElementById('vidareUtr').innerHTML = "Fortsatt utredning:" + " " + test;
    document.getElementById('demoMinus').innerHTML = "Sannolikhet för kranskärlssjukdom efter normalt arbetsprov är " + probMinus + " %.";
    document.getElementById('demoPlus').innerHTML = "Sannolikhet för kranskärlssjukdom efter patologiskt arbetsprov är " + probPlus + " %.";
}


var d = 0;
var htn = 0;
var hlpd = 0;
var sm = 0;

function riskD() {
    var diaVal = document.getElementById("diabetes");
    if (diaVal.checked == true) {
        d = 1;
    } else {
        d = 0;
    }
}

function riskH() {
    var htnVal = document.getElementById("hypertoni");
    if (htnVal.checked == true) {
        htn = 1;
    } else {
        htn = 0;
    }
}

function riskL() {
    var hlpdVal = document.getElementById("hyperlipidemi");
    if (hlpdVal.checked == true) {
        hlpd = 1;
    } else {
        hlpd = 0;
    }
}

function riskSold() {
    var smVal = document.getElementById("smokingOld");
    if (smVal.checked == true) {
        sm = 1;
    } else {
        sm = 0;
    }
}


function cadScoreN() {
    var aN = document.getElementById("ageN").value;
    var sN = document.getElementById("sexN").value;
    var angN = $('input[name="anginaTypN"]:checked').length;

    if (angN == 0 && aN.length == 0 && andning.checked == false) {
        document.getElementById("addInfoN").innerHTML = "Komplettera med beskrivning av symtom och riskfaktorer.";
        document.getElementById('ptpAnginaN').innerHTML = "";
        return;
    } if (aN.length == 0) {
        document.getElementById("addInfoN").innerHTML = "Komplettera med beskrivning av riskfaktorer.";
        document.getElementById('ptpAnginaN').innerHTML = "";
        return;
    } if (angN == 0 && andning.checked == false) {
        document.getElementById("addInfoN").innerHTML = "Komplettera med beskrivning av symtom.";
        document.getElementById('ptpAnginaN').innerHTML = "";
        return;
    }

    //Copenhagen model
    var angResultChg;
    if (andning.checked == false) {
        switch (angN) {
            case 0:
                angResultChg = 0;
                break;
            case 1:
                angResultChg = 0;
                break;
            case 2:
                angResultChg = 1.7839;
                break;
            case 3:
                angResultChg = 2.7699;
        }
    } else {
        angResultChg = 0;
    }

    var dyspneChg;
    if (andning.checked == true) {
        dyspneChg = 0.8071;
    } else {
        dyspneChg = 0;
    }

    var hereditChg;
    if (heredit.checked == true) {
        hereditChg = 0.4394;
    } else {
        hereditChg = 0;
    }
    
    var dysLipChg;
    if (dyslipid.checked == true) {
        dysLipChg = 0.9551;
    } else {
        dysLipChg = 0;
    }
    
    var diaMelChg;
    if (diabet.checked == true) {
        diaMelChg = 0.3767;
    } else {
        diaMelChg = 0;
    }
    
    //CAD Consortium model
    var angResultCad;
    switch (angN) {
        case 1:
            angResultCad = 0;
            break;
        case 2:
            angResultCad = 0.633;
            break;
        case 3:
            angResultCad = 1.998;
    }

    var diaValCad;
    if (diabet.checked == true) {
        diaValCad = 0.828;
    } else {
        diaValCad = 0;
    }

    var typDiabCad;
    if (angN == 3 && diabet.checked == true) {
    typDiabCad = -0.402;
    } else {
    typDiabCad = 0;
    }

    var htnValCad;
    if (hptn.checked == true) {
        htnValCad = 0.338;
    } else {
        htnValCad = 0;
    }

    var hlpdValCad;
    if (dyslipid.checked == true) {
        hlpdValCad = 0.422;
    } else {
        hlpdValCad = 0;
    }

    var smValCad;
    if (smoking.checked == true) {
        smValCad = 0.461;
    } else {
        smValCad = 0;
    }


    var logitChg = 0.5031 * aN / 10 + 1.4468 * sN + angResultChg + dyspneChg + hereditChg + dysLipChg + diaMelChg - 8.5499;
    oddsChg = Math.exp(logitChg);
    var probChg = Math.round(100 * oddsChg / (1 + oddsChg));

    var logitCad= 0.062 * aN + 1.332 * sN + angResultCad + diaValCad + typDiabCad + htnValCad + hlpdValCad + smValCad - 7.539;
    oddsCad = Math.exp(logitCad);
    var probCad = Math.round(100 * oddsCad / (1 + oddsCad));


    if (andning.checked == true && probChg < 15) {
        document.getElementById('btnPtpN').style.backgroundColor = "green";
    } else if (andning.checked == true && (probChg >= 15 && probChg < 85)) {
        document.getElementById('btnPtpN').style.backgroundColor = "orange";
    } else if (andning.checked == true && (probChg > 85)){
        document.getElementById('btnPtpN').style.backgroundColor = "red";
    } else if (probCad < 15 && probChg < 15) {
        document.getElementById('btnPtpN').style.backgroundColor = "green";
    } else if (probCad > 85 || probChg > 85) {
        document.getElementById('btnPtpN').style.backgroundColor = "red";
    } else {
        document.getElementById('btnPtpN').style.backgroundColor = "orange";
    }

    if (andning.checked == true) {
        document.getElementById('ptpAnginaN').innerHTML = "";
    } else {
        document.getElementById('ptpAnginaN').innerHTML = "CAD Consortium clinical score: " + probCad + " %.";
    }

    document.getElementById("addInfoN").innerHTML = "";
    
    document.getElementById('ptpAnginaChg').innerHTML = "Copenhagen studie* score: " + probChg + " %.";
}

function clearAnginaN() {
    document.getElementById("ageN").value = "";
    document.getElementById('sexN').value = "1";
    document.getElementById('ptpAnginaN').innerHTML = "";
    document.getElementById('ptpAnginaChg').innerHTML = "";
    document.getElementById('addInfoN').innerHTML = "";
    $('#anginaNyaMod').find('input:checkbox').prop('checked', false);
    document.getElementById('btnPtpN').style.backgroundColor = "lightslategrey";
}

$(function () {
    $('#andning').click(function () {
        $('#anTyp').find('input:checkbox').prop('checked', false);
    });
});

$(function () {
    $('#anTyp').click(function () {
        $('#andBesv').find('input:checkbox').prop('checked', false);
    });
});

function calculateBmi(Atext, Btext, form) {
    var A = parseFloat(Atext);
    var B = parseFloat(Btext);
    form.Answer.value = Math.round(A * 10000 / (B * B));
}

function clearForm(form) {
    form.input_A.value = "";
    form.input_B.value = "";
    form.Answer.value = "";
}

function nextProv() {
    var tidEfter = document.getElementById("tid").value;
    tidEfter = tidEfter.replace(/,/g, '.');
    var tropT = document.getElementById("troponin2").value;
    var resultat;
    if (tidEfter.length == 0) {
        resultat = "Fyll i förfluten tid efter senaste episoden med symtom";
    } else if (tidEfter < 1 || tropT > 1) {
        resultat = "Nästa Troponin T ska tas om 3 timmar";
    } else if (tidEfter >= 1 && tidEfter < 2) {
        resultat = "Nästa Troponin T ska tas om 1 timme";
    } else if (tidEfter >= 2 && tidEfter < 8) {
        if (tropT == 0) {
            resultat = "Det behövs inte ta flera prover";
        } else {
            resultat = "Nästa Troponin T ska tas om 1 timme";
        }
    } else if (tidEfter >= 8) {
        resultat = "Det behövs inte ta flera prover";
    }
    document.getElementById("svarTrop").innerHTML = resultat;
}

function graceScore() {
    var a = document.getElementById("yrs").value;
    var p = document.getElementById("puls").value;
    var b = document.getElementById("bltr").value;
    var krea = document.getElementById("krea").value;
    var k = krea / 88.4;
    var ki = document.getElementById("killip").value;
    var x = document.getElementById("arrest").checked;
    var hs;
    if (x == true) {
        hs = "1";
    }
    else {
        hs = "0";
    }
    var y = document.getElementById("mark").checked;
    var tnt;
    if (y == true) {
        tnt = "1";
    }
    else {
        tnt = "0";
    }
    var z = document.getElementById("st").checked;
    var dev;
    if (z == true) {
        dev = "1";
    }
    else {
        dev = "0";
    }


    var asc;
    if (a < 30) { asc = 0; }
    else if (30 <= a && a < 40) { asc = 8; }
    else if (40 <= a && a < 50) { asc = 25; }
    else if (50 <= a && a < 60) { asc = 41; }
    else if (60 <= a && a < 70) { asc = 58; }
    else if (70 <= a && a < 80) { asc = 75; }
    else if (80 <= a && a < 90) { asc = 91; }
    else { asc = 100; }

    var psc;
    if (p < 50) { psc = 0; }
    else if (50 <= p && p < 70) { psc = 3; }
    else if (70 <= p && p < 90) { psc = 9; }
    else if (90 <= p && p < 110) { psc = 15; }
    else if (110 <= p && p < 150) { psc = 24; }
    else if (150 <= p && p < 200) { psc = 38; }
    else { psc = 46; }

    var bsc;
    if (b < 80) { bsc = 58; }
    else if (80 <= b && b < 100) { bsc = 53; }
    else if (100 <= b && b < 120) { bsc = 43; }
    else if (120 <= b && b < 140) { bsc = 34; }
    else if (140 <= b && b < 160) { bsc = 24; }
    else if (160 <= b && b < 200) { bsc = 10; }
    else { bsc = 0; }

    var ksc;
    if (k < 0.4) { ksc = 1; }
    else if (0.4 <= k && k < 0.8) { ksc = 4; }
    else if (0.8 <= k && k < 1.2) { ksc = 7; }
    else if (1.2 <= k && k < 1.6) { ksc = 10; }
    else if (1.6 <= k && k < 2.0) { ksc = 13; }
    else if (2.0 <= k && k < 4.0) { ksc = 21; }
    else { ksc = 28; }

    var kisc;
    if (ki == 1) { kisc = 0; }
    else if (ki == 2) { kisc = 20; }
    else if (ki == 3) { kisc = 39; }
    else { kisc = 59; }

    var graceSum = asc + psc + bsc + ksc + kisc + 39 * hs + 14 * tnt + 28 * dev;

/*
    var kisc;
    if (ki == 1) { kisc = 0; }
    else if (ki == 2) { kisc = 20; }
    else if (ki == 3) { kisc = 39; }
    else { kisc = 59; }

    var bsc;
    if (b < 80) { bsc = 58; }
    else if (80 <= b && b < 100) { bsc = 58 -(b-80)*(10/20); }
    else if (100 <= b && b < 110) { bsc = 48 -(b-100)*(5/10); }
    else if (110 <= b && b < 120) { bsc = 43 -(b-110)*(4/10); }
    else if (120 <= b && b < 130) { bsc = 39 -(b-120)*(5/10); }
    else if (130 <= b && b < 140) { bsc = 34 -(b-130)*(5/10); }
    else if (140 <= b && b < 150) { bsc = 29 -(b-140)*(5/10); }
    else if (150 <= b && b < 160) { bsc = 24 -(b-150)*(5/10); }
    else if (160 <= b && b < 180) { bsc = 19 -(b-160)*(9/20); }
    else if (180 <= b && b < 200) { bsc = 10 -(b-180)*(10/20); }
    else { bsc = 0; }

    var psc;
    if (p < 50) { psc = 0; }
    else if (50 <= p && p < 60) { psc = 0 + (p-50)*(3/10); }
    else if (60 <= p && p < 70) { psc = 3 + (p-60)*(3/10); }
    else if (70 <= p && p < 80) { psc = 6 + (p-70)*(3/10); }
    else if (80 <= p && p < 90) { psc = 9 + (p-80)*(3/10); }
    else if (90 <= p && p < 100) { psc = 12 + (p-90)*(3/10); }
    else if (100 <= p && p < 110) { psc = 15 + (p-100)*(3/10); }
    else if (110 <= p && p < 150) { psc = 18 + (p-110)*(12/40); }
    else if (150 <= p && p < 200) { psc = 30 + (p-150)*(16/50); }
    else { psc = 46; }

    var asc;
    if (a < 30) { asc = 0; }
    else if (30 <= a && a < 40) { asc = 0 + (a-30)*(17/10); }
    else if (40 <= a && a < 50) { asc = 17 + (a-40)*(16/10); }
    else if (50 <= a && a < 60) { asc = 33 + (a-50)*(17/10); }
    else if (60 <= a && a < 70) { asc = 50 + (a-60)*(17/10); }
    else if (70 <= a && a < 80) { asc = 67 + (a-70)*(16/10); }
    else if (80 <= a && a < 90) { asc = 83 + (a-80)*(17/10); }
    else { asc = 100; }

    var ksc;
    if (k < 0.2) { ksc = 0 + (k-0)*(1/0.2); }
    else if (0.2 <= k && k < 0.4) { ksc = 1 + (k-0.2)*(2/0.2); }
    else if (0.4 <= k && k < 0.6) { ksc = 3 + (k-0.4)*(1/0.2); }
    else if (0.6 <= k && k < 0.8) { ksc = 4 + (k-0.6)*(2/0.2); }
    else if (0.8 <= k && k < 1.0) { ksc = 6 + (k-0.8)*(1/0.2); }
    else if (1.0 <= k && k < 1.2) { ksc = 7 + (k-1.0)*(1/0.2); }
    else if (1.2 <= k && k < 1.4) { ksc = 8 + (k-1.2)*(2/0.2); }
    else if (1.4 <= k && k < 1.6) { ksc = 10 + (k-1.4)*(1/0.2); }
    else if (1.6 <= k && k < 1.8) { ksc = 11 + (k-1.6)*(2/0.2); }
    else if (1.8 <= k && k < 2.0) { ksc = 13 + (k-1.8)*(1/0.2); }
    else if (2.0 <= k && k < 3.0) { ksc = 14 + (k-2.0)*(7/1); }
    else if (3.0 <= k && k < 4.0) { ksc = 21 + (k-3.0)*(7/1); }
    else { ksc = 28; }

    var graceSum = Math.round(kisc + bsc + psc + asc + ksc + 28 * dev + 14 * tnt + 39 * hs);
*/
    var patR = patRisk();
    function patRisk() {
        if (graceSum < 140) {
            patR = "";
            document.getElementById('btnGscore').style.backgroundColor = "green";
            document.getElementById('gScore').style.borderColor = "green";
        } else {
            patR = "Hög kardiovaskulär risk, överväg coronarutredning under närmaste dygnet.";
            document.getElementById('btnGscore').style.backgroundColor = "red";
            document.getElementById('gScore').style.borderColor = "red";
        }
        return patR;
    }

    if (a == 0 || p == 0 || b == 0 || krea == 0) {
        document.getElementById("gScore").innerHTML = "Fyll i alla parametrar.";
        document.getElementById('ptpAngina').innerHTML = "";
        document.getElementById('demoMinus').innerHTML = "";
        document.getElementById('demoPlus').innerHTML = "";
        return;
    }
    document.getElementById("gScore").innerHTML = "GRACE score" + " " + graceSum + "." + " " + patR;
}

function clearGrace(form) {
    form.yrs.value = "";
    form.puls.value = "";
    form.bltr.value = "";
    form.krea.value = "";
    form.killip.value = "1";
    $('#graceCalc').find('input:checkbox').prop('checked', false);
    $('#graceCalc').find('input:checkbox').prop('checked', false);
    $('#graceCalc').find('input:checkbox').prop('checked', false);

    document.getElementById("gScore").innerHTML = "";
    document.getElementById('btnGscore').style.backgroundColor = "lightslategrey";
    document.getElementById('gScore').style.borderColor = "";
}